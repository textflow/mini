#!/usr/bin/perl -w
# 
# template processor for Xelera TextFlow
# (c) Xelera, 2007-2009
# License: GNU Affero GPL version 3 or later
# Authors:
#  * andrea rota <a@xelera.eu>
#  * Giovanni Biscuolo <g@xelera.eu>

use strict;
use warnings;

use Getopt::Long;
use Template;
use YAML qw'LoadFile';
use Data::Dumper;


my $yamlFile = undef;
my $ttFile = undef;
my $verbose = undef;

GetOptions(
 'verbose' => \$verbose,
 'text=s' => \$yamlFile,
 'template=s' => \$ttFile,
);

unless($yamlFile and $ttFile) {
    die "Syntax: tf-apply [-v] --template <template_file> --text <yaml_file>\n";
}

my @yaml = LoadFile($yamlFile);

# add template and text folder to Template Toolkit's INCLUDE_PATH
my $template_path = $ttFile;
if($template_path =~ m/\//) {
  $template_path =~ s/(.*)\/.*$/$1/;
} else {
  $template_path = '.';
}

my $text_path = $yamlFile;
if($text_path =~ m/\//) {
  $text_path =~ s/(.*)\/.*$/$1/;
} else {
  $text_path = '.';
}

print STDERR "Using $template_path and $text_path as additional sources of templates\n" if $verbose;

my $include_path = ['.', $template_path, $text_path];

my $tt = Template->new({
 INCLUDE_PATH => $include_path,
 RELATIVE => 1,
});

$tt->process($ttFile, $_) || die $tt->error(), "\n" foreach (@yaml);

